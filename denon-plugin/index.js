// @ts-check
var Service, Characteristic;

module.exports = function (homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("switch-plugin", "DenonAVRX1300W", denon)
}

// @ts-ignore
const net = require("net");
// @ts-ignore
const util = require("util");
// @ts-ignore
const fs = require("fs");

function denon(log, config) {
    this.log = log;
    this.host = config["host"];
    this.port = config["port"];
    this.commands = JSON.parse(fs.readFileSync("commands.json", "utf-8"));
    // this.events = JSON.parse(fs.readFileSync("events.json", "utf-8"));
    this.receiveQueue = [];
    this.sendQueue = [];
    this.strDecoder = new util.TextDecoder();
    console.log("Created Queues");

    this.tcpSocket = this.createTCPSocket(this.host, this.port);
    let me = this;

    this.tcpSocket.on("data", function (data) {
        let message = me.strDecoder.decode(data).replace(/\r/, "");
        me.receiveQueue.push(message);
    })

    this.inputs = [
        { inputParamID: "cbl", id: 1, name: "CBL/SAT", active: false },
        { inputParamID: "dvd", id: 2, name: "DVD", active: true },
        { inputParamID: "bluray", id: 3, name: "Blu-ray", active: true },
        { inputParamID: "game", id: 4, name: "Game", active: true },
        { inputParamID: "aux", id: 5, name: "AUX", active: false },
        { inputParamID: "mediaplayer", id: 6, name: "Media Player", active: false },
        { inputParamID: "ipod", id: 7, name: "iPod/USB", active: true },
        { inputParamID: "tv", id: 8, name: "TV Audio", active: false },
        { inputParamID: "tuner", id: 9, name: "Tuner", active: false },
        { inputParamID: "onlinemusic", id: 10, name: "Online Music", active: true },
        { inputParamID: "bluetooth", id: 11, name: "Bluetooth", active: true },
        { inputParamID: "internetradio", id: 12, name: "Internet Radio", active: true }
    ];


    this.sendInterval = setInterval(this.sendLoop.bind(this), 100);
    this.readInterval = setInterval(this.readLoop.bind(this), 10);

    this.powerState = false;
    this.mainZoneState = false;
    this.muteState = false;
    this.volume = 0;
    this.activeIdentifier = 1;

    this.services = [];
}

denon.prototype = {
    createTCPSocket: function () {
        let me = this;
        let tcpSocket = new net.Socket();
        console.log("Created new TCP socket.")
        tcpSocket.connect({ host: me.host, port: me.port }, () => console.log("TCP connected."));

        tcpSocket.on("timeout", function () {
            console.log("TCP timeout");
            tcpSocket.destroy();
        });

        tcpSocket.on("close", function () {
            console.log("TCP socket closed.")
            tcpSocket.destroy();
        });

        tcpSocket.on("error", function (err) {
            console.log("TCP Socket error " + err);
            tcpSocket.destroy();
            // setTimeout(() => me.tcpSocket = me.createTCPSocket(), 1000);
        })

        tcpSocket.on("data", function (data) {
            let rawMessage = me.strDecoder.decode(data);
            let messages = rawMessage.split("\r");
            let numMessages = messages.length;
            for (let i = 0; i < numMessages; i++) {
                if (messages[i] != "") {
                    me.receiveQueue.push(messages[i]);
                }
            }
        });

        return tcpSocket;
    },

    sendLoop: function () {
        if (this.sendQueue.length > 0) {
            if (this.tcpSocket.destroyed) {
                clearInterval(this.sendInterval);
                console.log("Reinitializing TCP Socket for Denon receiver.")
                this.tcpSocket = this.createTCPSocket();
                this.sendInterval = setInterval(this.sendLoop.bind(this), 200);
            }
            else {
                let msg = this.sendQueue.shift() + "\r";
                this.tcpSocket.write(msg);
                console.log("Sent: " + msg.replace("\r", ""));
            }
        }
    },

    readLoop: function () {
        if (this.receiveQueue.length > 0) {
            while (this.receiveQueue.length > 0) {
                let curMsg = this.receiveQueue.shift();
                console.log("Handling receive message " + curMsg.replace(/\r/, ""));
                let cmd = curMsg.substring(0, 2);
                let param = curMsg.substring(2);
                switch (cmd) {
                    case this.commands.power:
                        switch (param) {
                            case this.commands.powerPARAMS.on:
                                this.powerState = true;
                                break;
                            case this.commands.powerPARAMS.off:
                                this.powerState = false;
                                break;
                        }
                        break;

                    case this.commands.mainZone:
                        switch (param) {
                            case this.commands.mainZonePARAMS.on:
                                this.mainZoneState = true;
                                break;
                            case this.commands.mainZonePARAMS.off:
                                this.mainZoneState = false;
                                break;
                        }
                        break;

                    case this.commands.mute:
                        switch (param) {
                            case this.commands.mutePARAMS.on:
                                this.muteState = true;
                                break;
                            case this.commands.mutePARAMS.off:
                                this.muteState = false;
                                break;
                        }
                        break;

                    case this.commands.volume:
                        if (param.substring(0, 3) === "MAX") break;
                        switch (param) {
                            case this.commands.volume.up:
                                this.volume += 1;
                                break;
                            case this.commands.volume.down:
                                this.volume -= 1;
                                break;
                            default:
                                let _volume = Number.NaN;
                                if (param.length === 3) _volume = Number.parseInt(param.substring(0, 1));
                                else if (param.length === 2) _volume = Number.parseInt(param);
                                if (!isNaN(_volume)) {
                                    this.volume = _volume;
                                    console.log("set volume to " + this.volume);
                                }
                                break;
                        }
                        break;

                    case this.commands.input:
                        let inputParams = this.commands.inputPARAMS;
                        let newInput = this.inputs.filter(input => inputParams[input.inputParamID] === param);
                        if (newInput.length > 0) {
                            this.activeIdentifier = newInput[0].id;
                        }
                        break;
                }
            }
        }
    },

    getServices: function () {
        let informationService = new Service.AccessoryInformation();
        informationService
            .setCharacteristic(Characteristic.Manufacturer, "Denon")
            .setCharacteristic(Characteristic.Model, "AVR-X1300W")
            .setCharacteristic(Characteristic.SerialNumber, "123")
            .setCharacteristic(Characteristic.Name, "Denon")
            .setCharacteristic(Characteristic.ConfiguredName, "Denon");
        this.services.push(informationService);

        let tvService = new Service.Television();
        tvService.setCharacteristic(Characteristic.ConfiguredName, "DenonTV");
        tvService.setCharacteristic(Characteristic.SleepDiscoveryMode, Characteristic.SleepDiscoveryMode.ALWAYS_DISCOVERABLE)
        tvService
            .getCharacteristic(Characteristic.Active)
            .on("get", this.getPowerCharacteristic.bind(this))
            .on("set", this.setPowerCharacteristic.bind(this));
        tvService
            .getCharacteristic(Characteristic.RemoteKey)
            .on("set", this.sendRemoteKey.bind(this));
        tvService
            .getCharacteristic(Characteristic.ActiveIdentifier)
            .on("get", this.getActiveIdentifier.bind(this))
            .on("set", this.setActiveIdentifier.bind(this));
        this.services.push(tvService);
            
        let speakerService = new Service.TelevisionSpeaker();
        speakerService.setCharacteristic(Characteristic.Name, "Denon-volume");
        speakerService.setCharacteristic(Characteristic.Active, Characteristic.Active.ACTIVE);
        speakerService.setCharacteristic(Characteristic.VolumeControlType, Characteristic.VolumeControlType.ABSOLUTE);
        speakerService
            .getCharacteristic(Characteristic.Mute)
            .on("set", this.setMuteCharacteristic.bind(this));
        speakerService
            .getCharacteristic(Characteristic.VolumeSelector)
            .on("set", this.setVolumeCharacteristicInc.bind(this));
        tvService.addLinkedService(speakerService);
        this.services.push(speakerService);

        let volumeService = new Service.Fan();
        volumeService.setCharacteristic(Characteristic.Name, "Lautstärke");
        volumeService.getCharacteristic(Characteristic.On)
            .on("get", this.getMuteCharacteristicInv.bind(this))
            .on("set", this.setMuteCharacteristicInv.bind(this));
        volumeService.getCharacteristic(Characteristic.RotationSpeed)
            .on("get", this.getVolumeCharacteristic.bind(this))
            .on("set", this.setVolumeCharacteristic.bind(this));
        this.services.push(volumeService);


        this.inputs.forEach(input => {
            let inputService = this.createInputService(input);
            tvService.addLinkedService(inputService);
            this.services.push(inputService);
        })


        return this.services;
    },

    createInputService: function (input) {
        let inputService = new Service.InputSource(input.name, input.name + "-input");
        inputService.setCharacteristic(Characteristic.ConfiguredName, input.name);
        inputService.setCharacteristic(Characteristic.Name, input.name);
        inputService.setCharacteristic(Characteristic.IsConfigured, Characteristic.IsConfigured.CONFIGURED);
        inputService.setCharacteristic(Characteristic.InputSourceType, Characteristic.InputSourceType.HDMI);
        if (input.active) inputService.setCharacteristic(Characteristic.CurrentVisibilityState, Characteristic.CurrentVisibilityState.SHOWN);
        else inputService.setCharacteristic(Characteristic.CurrentVisibilityState, Characteristic.CurrentVisibilityState.HIDDEN);
        inputService.setCharacteristic(Characteristic.InputDeviceType, Characteristic.InputDeviceType.OTHER);
        inputService.setCharacteristic(Characteristic.Identifier, input.id);

        return inputService;
    },

    getPowerCharacteristic: function (callback) {
        console.log("Polling power");

        let msg = this.commands.power + this.commands.statusPARAM;
        this.sendQueue.push(msg);
        let me = this;
        setTimeout(() => callback(null, me.powerState), 300);
    },

    setPowerCharacteristic: function (powerState, callback) {
        console.log("Setting power");
        let msg = this.commands.mainZone;
        if (powerState) msg += this.commands.mainZonePARAMS.on;
        else msg += this.commands.mainZonePARAMS.off;

        this.sendQueue.push(msg);
        callback();
    },

    getMuteCharacteristicInv: function (callback) {
        console.log("Polling Mute Inverted");
        let me = this;
        console.log(!me.muteState);

        let msg = this.commands.mute + this.commands.statusPARAM;
        this.sendQueue.push(msg);
        setTimeout(() => callback(null, !me.muteState), 300);
    },
    getMuteCharacteristic: function (callback) {
        console.log("Polling Mute");

        let msg = this.commands.mute + this.commands.statusPARAM;
        this.sendQueue.push(msg);
        let me = this;
        setTimeout(() => callback(null, me.muteState), 300);
    },

    setMuteCharacteristicInv: function (invMuteState, callback) {
        console.log("Setting mute inverted");
        let msg = this.commands.mute;
        if (!invMuteState) msg += this.commands.mutePARAMS.on;
        else msg += this.commands.mutePARAMS.off;

        this.sendQueue.push(msg);
        callback();
    },
    setMuteCharacteristic: function (muteState, callback) {
        console.log("Setting mute");
        let msg = this.commands.mute;
        if (muteState) msg += this.commands.mutePARAMS.on;
        else msg += this.commands.mutePARAMS.off;

        this.sendQueue.push(msg);
        callback();
    },

    getVolumeCharacteristic: function (callback) {
        console.log("Polling volume");
        console.log("volume " + this.volume);

        let msg = this.commands.volume + this.commands.statusPARAM;
        this.sendQueue.push(msg);
        let me = this;
        setTimeout(() => callback(null, me.volume), 10);
    },

    setVolumeCharacteristic: function (volumeSet, callback) {
        console.log("setting volume " + volumeSet);
        let volumeString = "";
        if (volumeSet >= 100) volumeString = "99";
        else if (volumeSet === 0) return callback();
        else if (volumeSet < 10) volumeString = "0" + volumeSet;
        else volumeString = volumeSet.toString();
        let msg = this.commands.volume + volumeString;
        console.log("sending message " + msg)
        this.sendQueue.push(msg);
        callback();
    },

    setVolumeCharacteristicInc: function (volumeParam, callback) {
        if (volumeParam === 0) if (this.volume < 98) this.volume++;
        if (volumeParam === 1) if (this.volume > 0) this.volume--;
        console.log("setting volume: " + this.volume);

        let msg = this.commands.volume + ("0" + this.volume).slice(-2);
        this.sendQueue.push(msg);
        callback();
    },

    sendRemoteKey: function (remoteKey, callback) {
        console.log("remote key");
        switch (remoteKey) {
            case Characteristic.RemoteKey.ARROW_UP:
                this.sendQueue.push("MNCUP");
                break;
        }
        callback();
    },

    getActiveIdentifier: function (callback) {
        console.log("Polling active id");
        let me = this;

        let msg = this.commands.input + this.commands.statusPARAM;
        this.sendQueue.push(msg);
        setTimeout(() => callback(null, me.activeIdentifier), 50);
    },

    setActiveIdentifier: function (id, callback) {
        console.log("Setting active id " + id);
        let inputToSet = this.inputs.filter(input => input.id === id);
        if (inputToSet.length > 0) {
            let msg = this.commands.input + this.commands.inputPARAMS[inputToSet[0].inputParamID];
            this.sendQueue.push(msg);
            this.activeIdentifier = id;
        }
        callback();
    }
}