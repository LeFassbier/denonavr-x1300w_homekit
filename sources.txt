On how to create homebridge plugins: https://blog.theodo.com/2017/08/make-siri-perfect-home-companion-devices-not-supported-apple-homekit/

Apple HomeKit documentation:
    root: https://developer.apple.com/documentation/homekit
    characteristicTypes: https://developer.apple.com/documentation/homekit
    